const express = require('express');
const path = require('path');

const app = express();

// Serve only the static files form the dist directory
app.use(express.static('./.well-known/brave-rewards-verification.txt'));

app.get('/.well-known/brave-rewards-verification.txt', function(req,res) {

  res.sendFile(path.join(__dirname,'/.well-known/brave-rewards-verification.txt'));
});

// Start the app by listening on the default Heroku port
app.listen(process.env.PORT || 8080);
