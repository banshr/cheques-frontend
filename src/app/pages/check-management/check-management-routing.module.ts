import { NgModule } from '@angular/core';
import { OutstandingComponent } from './outstanding/outstanding.component';
import { Routes, RouterModule } from '@angular/router';
import { InsertComponent } from './insert/insert.component';
import { PaidComponent } from './paid/paid.component';
import { EditComponent } from './edit/edit.component';
import { CheckService } from '../../services/check.service';

export const routes: Routes = [
  {path: 'outstanding', component: OutstandingComponent},
  {path: 'paid', component: PaidComponent},
  {path: 'insert', component: InsertComponent},
  {path: 'edit/:id', component: EditComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [CheckService]
})
export class CheckManagementRoutingModule {}

