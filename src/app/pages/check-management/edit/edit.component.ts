import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CheckService } from '../../../services/check.service';
import { CheckSummaryModel, StatusEnum } from '../../../models/check.model';
import { DateFormatConverter, DateValidator } from '../../../utils';
import { ToastrService } from 'ngx-toastr';
import { ResponseMessageModel } from '../../../models/response.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpErrorResponse } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  public response: CheckSummaryModel = new CheckSummaryModel();
  public Status = StatusEnum;
  public date: string;
  public isLoading: boolean = false;
  public editForm: FormGroup;
  constructor(private route: ActivatedRoute, private checkService: CheckService,
              private toaster: ToastrService,  private modalService: NgbModal,
              private router: Router, private fb: FormBuilder) {}

  ngOnInit() {
    this.editForm = this.fb.group({
      edit: ['', [Validators.required, DateValidator]]
    });
    const id = this.route.snapshot.paramMap.get('id');
    this.checkService.getBydId(id).then((resp: CheckSummaryModel) => {
      this.editForm.controls.edit.setValue(DateFormatConverter.backToFront(resp.date));
      this.response = resp;
    }).catch((err: HttpErrorResponse) => {
      if (err.status === 404) {
        this.toaster.error('Cheque não encontrado.', 'Erro!', { timeOut: 5000, closeButton: true, progressBar: true });
        this.router.navigateByUrl('check-management/outstanding');
      }
    });
  }

  private toasterSucesso(message): void {
    this.toaster.success(message, 'Sucesso!', { timeOut: 5000, closeButton: true, progressBar: true });
  }
  public saveChanges(id, dateString: string): void {
    this.isLoading = true;
    const body = {
      date: DateFormatConverter.frontToBack(dateString)
    };
    this.checkService.putNewDate(id, body).then((resp: ResponseMessageModel) => {
      this.toasterSucesso(resp.message);
      this.router.navigate(['/check-management/outstanding'], {queryParams: {returning: true}});
    }).finally(() => {
      this.isLoading = false;
    });
  }

  public checkDelete(id): void {
    this.isLoading = true;
    this.checkService.deleteCheck(id).then((resp: any) => {
      this.modalService.dismissAll();
      this.toasterSucesso(resp.message);
      this.router.navigate(['/check-management/outstanding'], {queryParams: {returning: true}});
    }).catch((err) => {
      console.log(err);
    }).finally(() => {
      this.isLoading = false;
    });
  }

  public confirm(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', centered: true });
  }
}
