import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BASE_URL } from '../utils';

@Injectable()
export class CheckService {
  constructor(private http: HttpClient) {}

  getByStatus(status): Promise<any> {
    const url: string = `${BASE_URL}/check/by_status/${status}`;
    return this.http.get(url).toPromise();
  }

  getBydId(id): Promise<any> {
    const url: string = `${BASE_URL}/check/by_id/${id}`;
    return this.http.get(url).toPromise();
  }

  putNewDate(id, date): Promise<any> {
    const url: string = `${BASE_URL}/check/new_date/${id}`;
    return this.http.put(url, date).toPromise();
  }

  deleteCheck(id): Promise<any> {
    const url: string = `${BASE_URL}/check/by_id/${id}`;
    return this.http.delete(url).toPromise();
  }
}
