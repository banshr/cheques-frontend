import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';

export const BASE_URL: string = 'https://cheques-backend.herokuapp.com';

@Injectable()
export class DateFormatConverter {
  static backToFront(dateString: string): string {
    return `${dateString.slice(8, 10)}${dateString.slice(5, 7)}${dateString.slice(0, 4)}`;
  }

  static frontToBack(dateString: string): string {
    return`${dateString.slice(4, 8)}-${dateString.slice(2, 4)}-${dateString.slice(0, 2)}`;
  }
}

export function DateValidator(control: AbstractControl): { [key: string]: boolean } | null {
  const day = Number(control.value.slice(0, 2));
  const month = Number(control.value.slice(2, 4));
  const year = Number(control.value.slice(4, 8));

  if (!/^\d{8}$/.test(control.value)) {
    return {dateValidator: true};
  }

  if (year < 1000 || year > 3000 || month === 0 || month > 12) {
    return {dateValidator: true};
  }

  const monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];


  if (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)) {
    monthLength[1] = 29;
  }

  if (!(day > 0 && day <= monthLength[month - 1])) {
    return {dateValidator: true};
  }

  return null;
}
